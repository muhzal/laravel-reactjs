let defaultState = {
    datas: [],
    data: null,
    fetched: false,
    error: null
}

export default function reducer(state = {
    datas: [],
    page: 1,
    totalData:0,
    data: null,
    fetched: false,
    error: null
} , action) {
    switch (action.type) {
        case "FETCH_USERS_REJECTED":
        {
            return {
                ...state,
                fetched: false,
                error: action.payload
            }
        }
        case "FETCH_USERS_FULFILLED":
        {
            const { datas, page, totalData, perPage } = action.payload;
            return {
                ...state,
                fetched: true,
                datas,
                page,
                totalData,
                perPage
            }
        }
        case "FETCH_USER_REJECTED":
        {
            return {
                ...state,
                fetched: false,
                error: action.payload
            }
        }
        case "FETCH_USER_FULFILLED":
        {
            return {
                ...state,
                fetched: true,
                data: action.payload
            }
        }
    }
    return state;
}
