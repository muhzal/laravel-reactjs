# Laravel React #

This is an example project how to use reactjs as views and laravel just return a data as API

### How to Install ? ###
* install composer dependecies
```
#!php
composer install
```
* install npm dependecies
```
#!php
npm install
```
* make .env file
```
#!php
php -r "file_exists('.env') || copy('.env.example', '.env');"
```

* generate key
```
#!php
php artisan key:generate
```
* ajust database connection on .env file
* build reactjs commponent
```
#!php
npm run watch
```
* run laravel
```
#!php
php artisan serve
```