<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('api/v1/users',function(){
    $users = factory(App\User::class,50)->make();
    $page  = request('page')?:1;    
    $total = $users->count();
    $users = $users->forPage($page,10)->values();
    return response()->json([
        'datas' => $users,
        'perPage'=>10,
        'page' => $page,
        'totalData' => $total,
    ]);
});

Route::get('{path?}/{path2?}/{path3?}', function () {
    return view('index');
});
